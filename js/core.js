document.body.onload = () => {
	if(!localStorage.getItem('saved')) {
		save();
	} else {
		renderOfSave();
	}
}

var render = (element, text) => {
	document.querySelector(element).innerHTML = text;
}

var isInteger = (num) => {
	return (num ^ 0) === num;
}

var rnd = (min, max) => {
	var rand = min - 0.5 + Math.random() * (max - min + 1)
	rand = Math.round(rand);
	return rand;
}

var save = () => {
	var parms = {
		'money': money,
		'ip': ip,
		'ipa': ipa,
		'ipg': ipg,
		'ipt': ipt,
		'autoIp': autoIp,
		'av': av,
		'pt': pt,
		'uh': uh,
		'ddos': ddos,
		'gl': gl,
		'jur': jur,
		'pd': pd,
		'fsb': fsb,
		'rb': rb,
		'priceAv': priceAv,
		'pricePt': pricePt,
		'priceUh': priceUh,
		'priceDdos': priceDdos,
		'priceGl': priceGl,
		'priceJur': priceJur,
		'pricePd': pricePd,
		'priceFsb': priceFsb,
		'priceRb': priceRb,
	}

	localStorage.setItem('saved', JSON.stringify(parms));
}
var renderOfSave = () => {
		var data = JSON.parse(localStorage.getItem('saved'));
		
		money = data.money;
		ip = data.ip;
		ipa = data.ipa;
		ipg = data.ipg;
		ipt = data.ipt;
		autoIp = data.autoIp;
		av = data.av;                         // Автоматизация выгрузки
		pt = data.pt;                         // Письменные требования 
		uh = data.uh;                         // Увеличение штата
		ddos = data.ddos;                     // DDOS новостных сми
		gl = data.gl;                         // Горячая линия
		jur = data.jur;                       // Жаров у руля
		pd = data.pd;                         // Побег Дурова
		fsb = data.fsb;                       // Сдать ip ФСБ
		rb = data.rb;                         // Распилить бюджет
		priceAv = data.priceAv;               // Автоматизация выгрузки
		pricePt = data.pricePt;               // Письменные требования 
		priceUh = data.priceUh;               // Увеличение штата
		priceDdos = data.priceDdos;           // DDOS новостных сми
		priceGl = data.priceGl;               // Горячая линия
		priceJur = data.priceJur;             // Жаров у руля
		pricePd = data.pricePd;               // Побег Дурова
		priceFsb = data.priceFsb;             // Сдать ip ФСБ
		priceRb = data.priceRb;               // Распилить бюджет
}

var fullScreen = (element) => {
	if (element.requestFullscreen) {
		element.requestFullscreen();
	} else if (element.mozRequestFullScreen) {
		element.mozRequestFullScreen();
	} else if (element.webkitRequestFullscreen) {
		element.webkitRequestFullscreen();
	} else if (element.msRequestFullscreen) {
		element.msRequestFullscreen();
	}
}

document.querySelector('#full').onclick = fullScreen(document);


var checkNumber = (selector, data= ip) => { // сокращает числа
	if (data < 1000) {
		render(selector, Math.round(data.toString()));
	}
	if ((data / 1000) >= 1) {
		render(selector, Math.round(data.toString()[0])+'k');
	}
	if((data / 10000) >= 1) {
		render(selector, data.toString()[0]+ data.toString()[1]+'k');
	}
	if((data / 100000) >= 1) {
		render(selector, data.toString()[0] + data.toString()[1] +  data.toString()[2] +'k');
	}
	if((data / 1000000) >= 1) {
		render(selector, data.toString()[0] + 'm');
	}
	if((data / 10000000) >= 1) {
		render(selector, data.toString()[0]+ data.toString()[1]+'m');
	}
	if((ip / 100000000) >= 1) {
		render(selector, data.toString()[0] + data.toString()[1] +  data.toString()[2] +'m');
	}
	if((data / 1000000000) >= 1) {
		render(selector, data.toString()[0] +'t');
	}
	if((data / 10000000000) >= 1) {
		render(selector, data.toString()[0] + data.toString()[1] +'t');
	}
	if((data / 100000000000) >= 1) {
		render(selector, data.toString()[0] + data.toString()[1] + + idatatoString()[2] + 't');
	}
}