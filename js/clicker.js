/* INIT */

setInterval(() => { // RENDER
	render('#av', Math.round(priceAv));
	render('#pt', Math.round(pricePt));
	render('#uh', Math.round(priceUh));
	render('#ddos', Math.round(priceDdos));
	render('#gl', Math.round(priceGl));
	render('#jur', Math.round(priceJur));
	render('#pd', Math.round(pricePd));

	render('#fsb', Math.round(priceFsb));
	render('#rb', Math.round(priceRb));
	render('#pp', Math.round(pricePp));
	render('#prd', Math.round(pricePp));
	render('#bblock', Math.round(priceBblock));
	render('#zo', Math.round(priceZo));
	

	allRender();
	
}, 200);

/* FUNCTIONS CLICK*/

document.querySelector('#clickIp').onclick = () => {
	ip++;
	countIp++;
	render('#ip', ip);
	save();
	//chekIp();
	checkCount();
	allRender();
}

/* TRANSACTION MONEY -> AUTO_IP */

document.querySelector('#buyAv').onclick = () => {
	if(money < priceAv) {
		alert('У вас недостаточно Денег');
	} else {
		Math.round(money -= priceAv);
		autoIp += av;
		
		priceAv *= 1.4;
		render('#av', Math.round(priceAv));
		render('#automaticIp', autoIp);
		
		allRender();
		save();
	}
}

document.querySelector('#buyPt').onclick = () => {
	if(money < pricePt) {
		alert('У вас недостаточно Денег');
	} else {
		money -= Math.round(pricePt);
		autoIp += pt;
		
		pricePt *= 1.4;
		render('#pt', Math.round(pricePt));
		render('#automaticIp', autoIp);
		
		allRender();
		save();
	}
}

document.querySelector('#buyUh').onclick = () => {
	if(money < priceUh) {
		alert('У вас недостаточно Денег');
	} else {
		money -= Math.round(priceUh);
		autoIp += uh;
		
		priceUh *= 1.4;
		render('#uh', Math.round(priceUh));
		render('#automaticIp', autoIp);
		
		allRender();
		save();
	}
}

document.querySelector('#buyDdos').onclick = () => {
	if(money < priceDdos) {
		alert('У вас недостаточно Денег');
	} else {
		money -= Math.round(priceDdos);
		autoIp += ddos;
		
		priceDdos *= 1.4;
		render('#ddos', Math.round(priceDdos));
		render('#automaticIp', autoIp);
		
		allRender();
		save();
	}
}

document.querySelector('#buyGl').onclick = () => {
	if(money < priceGl) {
		alert('У вас недостаточно Денег');
	} else {
		money -= Math.round(priceGl);
		autoIp += gl;
		
		priceGl *= 1.4;
		render('#gl', Math.round(priceGl));
		render('#automaticIp', autoIp);
		
		allRender();
		save();
	}
}

document.querySelector('#buyJur').onclick = () => {
	if(money < priceJur) {
		alert('У вас недостаточно Денег');
	} else {
		money -= Math.round(priceJur);
		autoIp += jur;
		
		priceJur *= 1.4;
		render('#jur', Math.round(priceJur));
		render('#automaticIp', autoIp);
		
		allRender();
		save();
	}
}

document.querySelector('#buyPd').onclick = () => {
	if(money < pricePd) {
		alert('У вас недостаточно Денег');
	} else {
		money -= Math.round(pricePd);
		autoIp += pd;
		
		pricePd *= 1.4;
		render('#pd', Math.round(pricePd));
		render('#automaticIp', autoIp);
		
		allRender();
		save();
	}
}

/* TRANSACTION IP -> MONEY */

document.querySelector('#buyFsb').onclick = () => {
	if(ip < priceFsb) {
		alert('У вас недостаточно Ip');
	} else {
		ip -= Math.round(priceFsb);
		money += fsb;
		
		render('#money', money);
		
		allRender();
		save();
	}
}

document.querySelector('#buyRb').onclick = () => {
	if(ip < priceRb) {
		alert('У вас недостаточно Ip');
	} else {
		ip -= Math.round(priceRb);
		money += rb;
		
		render('#money', money);
		
		allRender();
		save();
	}
}

document.querySelector('#buyPp').onclick = () => {
	if(ip < pricePp) {
		alert('У вас недостаточно Ip');
	} else {
		ip -= Math.round(pricePp);
		money += pp;
		
		render('#money', money);
		
		allRender();
		save();
	}
}

document.querySelector('#buyPrd').onclick = () => {
	if(ip < pricePrd) {
		alert('У вас недостаточно Ip');
	} else {
		ip -= Math.round(pricePrd);
		money += prd;
		
		render('#money', money);
		
		allRender();
		save();
	}
}

document.querySelector('#buyBblock').onclick = () => {
	if(ip < priceBblock) {
		alert('У вас недостаточно Ip');
	} else {
		ip -= Math.round(priceBblock);
		money += bblock;
		
		render('#money', money);
		
		allRender();
		save();
	}
}

document.querySelector('#buyZo').onclick = () => {
	if(ip < priceZo) {
		alert('У вас недостаточно Ip');
	} else {
		ip -= Math.round(priceZo);
		money += zo;
		
		render('#money', money);
		
		allRender();
		save();
	}
}

/* FUNCTION AUTO */

setInterval(() => {
	ip += autoIp;
	
	render('#automaticIp', autoIp);
	//chekIp();
	allRender();
	save();
}, 1000);

setInterval(() => {
	if (ip > 300) {
		let eventesNum = rnd(0, eventes.length-1);
		ip = ip - ((Math.round(ip/100)) * eventes[eventesNum][1]);
		alert(eventes[eventesNum][0]);
	}
	
	save();
}, rnd(5, 15)+'0000');

/* LOGIC */

var checkCount = () => {
	if (countIp == 10) {
		ipa++;
		countIpa++;
		countIp = 0;
	}
	
	if (countIpa == 10) {
		ipg++;
		countIpg++;
		countIpa = 0;
	}
	
	if (countIpg == 10) {
		ipaz++;
		countIpaz++;
		countIpg = 0;
	}
	
	if (countIpaz == 10) {
		ipdo++;
		countIpdo++;
		countIpaz = 0;
	}
	
	if (countIpdo == 10) {
		ipbn++;
		countIpbn++;
		countIpdo = 0;
	}
	
	if (countIpbn == 10) {
		ipt++;
		countIpt++;
		countIpbn = 0;
	}

	if (countIpt == 10) {
		alert('Поздравляем, вы заблокировали 10 ip telegram');
	}
}

var chekIp = () => {
	
	if (isInteger(ip / 10) == true) { // IP Amazon
		ipa++;
		render('#ipamazon', ipa);
	}
	
	if (isInteger(ipa / 10) == true) { // IP Google
	    ipg++;
		render('#ipgoogle', ipg);
	}
	
	if (isInteger(ipg / 10) == true) { // IP Azure
		ipaz++;
		render('#ipazure', ipaz);
	}
	
 	if (isInteger(ipaz / 10) == true) { // IP Digital Ocean
		ipdo++;
		render('#ipdigital', ipdo);
	}
	
 	if (isInteger(ipdo / 10) == true) { // IP BattleNet
		ipbn++;
		render('#ipbattle', ipbn);
	}
	
	if (isInteger(ipbn / 100000) == true) {
		ipt++;
		render('#iptelegram', ipt);
	}
}

var allRender = () => {
	checkNumber('#ip', ip);
	checkNumber('#ipamazon', ipa);
	checkNumber('#ipgoogle', ipg);
	checkNumber('#ipazure', ipaz);
	checkNumber('#ipdigital', ipdo);
	checkNumber('#ipbattle', ipbn);
	checkNumber('#iptelegram', ipt);
	checkNumber('#money', money);
	checkNumber('#automaticIp', autoIp);
}